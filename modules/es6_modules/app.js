import * as greeter from 'greet'

greeter.greet();


// this code will not work

// Update: In Node 9, it is enabled behind a flag, and uses the .mjs extension.

// node --experimental-modules my-app.mjs
// While import is indeed part of ES6, it is unfortunately not yet supported in NodeJS by default, and has only very recently landed support in browsers.

// See browser compat table on MDN and this Node issue.

// From James M Snell's Update on ES6 Modules in Node.js (February 2017):

// Work is in progress but it is going to take some time — We’re currently looking at around a year at least.

// Until support shows up natively, you'll have to continue using classic require statements:

// const express = require("express");
// If you really want to use new ES6/7 features in NodeJS, you can compile it using Babel. Here's an example server.