var greet1 = require("./greet1");
greet1();

var greet2 = require("./greet2");
greet2.greet();

var greet3 = require("./greet3");
greet3.greet();
greet3.greeting = "Changed greet3";
greet3.greet();

var greet3b = require("./greet3");
greet3b.greet();  // here cached module is called for greet3b when we import ./greet3 again

var greet4 = require("./greet4");
// greet4.greet();  // it will not work we need to create new instance of greet4
var greet = new greet4();
greet.greet();

var greet5 = require("./greet5");
greet5();




