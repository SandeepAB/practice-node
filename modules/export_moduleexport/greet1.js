exports = function(){
    console.log("Hi form exports");
}

console.log(exports);           // [Function: exports]
console.log(module.exports);    // {}

//exports and module.exports are different pointers for same reference ie exports object
// when we change exports from empty object to any other like function in this case the reference will break and exports becomes new variable


//if you dont want to break the reference then use exports.anything as like in example greet2.js

// using module.exports instead of simply exports is a best practice 


