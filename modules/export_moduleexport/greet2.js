exports.greet = function(){
    console.log("Hi form exports");
}

console.log(exports);           //{ greet: [Function] }
console.log(module.exports);    //{ greet: [Function] }