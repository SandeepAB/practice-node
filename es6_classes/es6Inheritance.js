var eventEmitter = require('events');

class Greeter extends eventEmitter{
    constructor(name){
        super();
        this.greetting = `Hi, ${name}`
    }

    greet(){
        console.log(this.greetting);
        this.emit("greet");
    }
}

var grt1 = new Greeter("Sandeep");

grt1.on("greet",function(){
    console.log("Someone greeted");
});

grt1.greet();