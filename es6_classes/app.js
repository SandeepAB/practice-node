'use strict';

class Person {
    constructor(fName,lName) {
        this.firstName = fName,
        this.lastName = lName
    }

    greet(){
        console.log(`Hi ${this.firstName} ${this.lastName}`);
    }
}

var sandeep = new Person("Sandeep","Basetti");
sandeep.greet();

var aadhya = new Person("Aadhya","S");
aadhya.greet();