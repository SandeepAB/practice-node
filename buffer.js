var buff = new Buffer("Hello","utf8");

console.log(buff);
console.log(buff.toString());
console.log(buff.toJSON());
console.log(buff[2]);
buff.write("c");
console.log(buff.toString());

//Typed array

var buffer = new ArrayBuffer(8);
var view = new Int32Array(buffer);

view[0] = 10;
view[1] = 20;
view[2] = 30;

console.log(view); //Int32Array [ 10, 20 ] it will not display 3rd element because buffer is of size 8*8 and view is of 32 * 3