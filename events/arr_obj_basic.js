obj = {
    greet:"Hello"
}

console.log(obj.greet);
console.log(obj["greet"]);
var prop = "greet";
console.log(obj[prop]);


var arr = [];

arr.push(function(){
    console.log("function 0");
});

arr.push(function(){
    console.log("function 1");
});

arr.push(function(){
    console.log("function 2");
});

arr.forEach(function(item){
    item();
})