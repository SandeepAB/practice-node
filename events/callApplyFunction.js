var obj = {
    name : "Sandeep",
    greet : function(){
        console.log(`Hi, ${this.name}.`);
    },
    add : function(a,b){
        console.log(`a+b=${a+b}`);
    }
}

obj.greet();
obj.add(20,08);

obj.greet.call({name:"Ravi"});
obj.greet.apply({name:"Ravi"});

obj.add.call({name:"Ravi"},20,08);
obj.add.apply({name:"Ravi"},[20,08]);