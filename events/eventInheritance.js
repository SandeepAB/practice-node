//==============================================================================================================
var eventEmitter = require('events');


function Greeter(){
    eventEmitter.call(this);
    this.greetting  =  "Hello";
}

var util = require("util");

util.inherits(Greeter,eventEmitter);


Greeter.prototype.greet = function(){
    console.log(this.greetting);
    this.emit("greet");
}

var grt1 = new Greeter();

grt1.on("greet",function(){
    console.log("Someone greeted");
});

grt1.greet();