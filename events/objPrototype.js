var person = {
    firstName : "",
    lastName : "",
    greet : function(){
        return "Hi "+this.firstName+" "+this.lastName;
    }
}

var sandeep = Object.create(person);
sandeep.firstName = "Sandeep";
sandeep.lastName = "Basetti";

aadhya = Object.create(person);
aadhya.firstName = "Aadhya";
aadhya.lastName = "S";

console.log(sandeep.greet());
console.log(aadhya.greet());
