var util = require("util");

function Person(){
    this.fName="Sandeep",
    this.lName="Basetti"
}

Person.prototype.greet = function(){
    console.log(`Hi, ${this.fName} ${this.lName}`);
}

function Engneer(){
    // Person.call(this);  
    this.branch = "ECE";
}

util.inherits(Engneer,Person);

var talent = new Engneer();
talent.greet();
