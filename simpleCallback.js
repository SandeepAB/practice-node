function greet(callback){
    console.log("Hello....");
    var person = {
        name  : "User"
    }
    callback(person);
}

greet(function(data){
   console.log(`Hi ${data.name}, I am callback`);
});

greet(function(data){
    console.log(`Hi ${data.name}, I am different callback`);
});