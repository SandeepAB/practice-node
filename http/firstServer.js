var http = require("http");
var fs = require("fs");

// http.createServer(function(req,res){
//     res.writeHead(200,{"Content-Type":"text/plain"});
//     res.end("Hello World");
// }).listen(3000,"127.0.0.1");


// http.createServer(function(req,res){
//     res.writeHead(200,{"Content-Type":"text/html"});
//     var html = fs.readFileSync("./home.html","utf8");
//     html = html.replace("{world}","Sandeep")
//     res.end(html);
// }).listen(3000,"127.0.0.1");

http.createServer(function (req, res) {

    if (req.url == "/") {
        res.writeHead(200, { "Content-Type": "text/html" });
        fs.createReadStream("./home.html", "utf8").pipe(res);  // from readale stream we can just pipe the response
        // html = html.replace("{world}", "Sandeep")
        // res.end(html);
    }
    else if(req.url == "/api"){
        var obj = {
            fName:"Sandeep",
            lName:"Basetti"
        }
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(JSON.stringify(obj));
    }
    else{
        res.writeHead(404);
        res.end();
    }
}).listen(3000, "127.0.0.1");