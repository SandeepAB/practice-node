var fs = require("fs");

var readable = fs.createReadStream(__dirname + '/greet.txt', {
    encoding: "utf8", highWaterMark: 16 * 1024
});

var writtable = fs.createWriteStream(__dirname + '/greetCopy.txt');

readable.on("data", function (chunk) {
    console.log(chunk.length);// default 64KB is this chunk size
    console.log(chunk);  // if encoding is not utf8 then chuck will not be in string that time use chunck.toString()
    writtable.write(chunk);
});


//==================================================================================================================
//  All streams are inherited by event streams.
//==================================================================================================================