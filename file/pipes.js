// Pipes can be performed on any streams

var fs = require("fs");

var readable = fs.createReadStream(__dirname+"/greet.txt");

var writable = fs.createWriteStream(__dirname+"/greetCopy2.txt");

readable.pipe(writable);


//============================================================================
var zlib = require("zlib");

var gzip = zlib.createGzip();

var compressed = fs.createWriteStream(__dirname+"/greet.txt.gz");

readable.pipe(gzip).pipe(compressed);