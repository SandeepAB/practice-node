var file = require("fs");

var syncData = file.readFileSync(__dirname+'/greet.txt',"utf8");
console.log(syncData);// this is blocking line so it executes after to above sync function  

file.readFile(__dirname+"/greet.txt","utf8",function(err,data){
    if(err)console.log(err);
    console.log(data);
});

console.log("last line of code"); // this is non blocking line so it executes paralell to above async function  
