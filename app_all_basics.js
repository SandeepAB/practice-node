console.log(" ========================================================================================================");
console.log("Basic");

var a=1;
var b=2;
var c=a+b;
console.log(c);
//==================================================================================================================
//  module and require
//==================================================================================================================
console.log(" =========================================================================================================");

console.log("module and require Example");

var greet = require("./organized_module/index");
greet.english();
greet.spanish();

//==================================================================================================================
//  Object
//==================================================================================================================
console.log(" ==================================================================================================================");

console.log("Object Example");

var person = {
    "firstName":"Sandeep",
    "lastName":"Bashetti",
    "greet":function(){
        console.log("Hi "+this.firstName+" "+this.lastName);
        return 1;
    }
};

console.log(person["firstName"]);

console.log(person.lastName);

console.log(person.greet());

//==================================================================================================================
//  functions and inheritance 
//==================================================================================================================
console.log(" ==================================================================================================================");

console.log("functions and inheritance ");

function Person (firstName,lastName){
    this.firstName = firstName;
    this.lastName = lastName;
}

Person.prototype.greet = function(){
    console.log("Hello "+this.firstName +" "+this.lastName);
}

Person.prototype.age = 18;
var Sandeep = new Person("Sandeep","Basetti");


Sandeep.greet();

var Ravi = new Person("Ravikiran","Bashetti");

Ravi.greet();

console.log(Sandeep.__proto__);

console.log(Ravi.__proto__===Sandeep.__proto__);

// var arina = new Person("Arina","Grande");
// arina.greet();

// console.log("Name "+arina.firstName+" "+arina.lastName+". "+"Age "+arina.age);
 
//==================================================================================================================
// Pass by value and pass by reference
//==================================================================================================================
console.log(" ==================================================================================================================");
console.log(" Pass by value and pass by reference");
//passby value
function change(b){
    b=2;
}
var a = 1;
console.log("Value of 'a' before change() called "+a);
change(a);
console.log("Value of 'a' after change() called "+a);

//passby reference
function changeObj(b){
  b.value = 2;
}

var obj = {};
obj.value = 1;

console.log("Value of 'obj.value' before changeObj() called "+obj.value);
changeObj(obj);
console.log("Value of 'obj.value' after changeObj() called "+obj.value);

//=====================================================================================================================
// Immediate invocation of function
//====================================================================================================================
console.log("====================================================================================================================");
console.log("Immediate invocation of function");
var name = "Saadhya";
console.log(name);
(function(Lname){
    var name = "Sandeep";
    console.log("Hi... I am "+name+" "+Lname)
}("Basetti"));      // Invocating immediate function
console.log(name);  // name variable has not changed after function because of scope




//=====================================================================================================================
// require is fuctions to which we pass path and module.exports is what the require function returns
//====================================================================================================================




//=====================================================================================================================
// 
//====================================================================================================================
console.log("====================================================================================================================");
console.log("");