var express = require("express");
var app = express();

var port = process.env.PORT || 3000

app.listen(port, function () {
    console.log("The server is listening on port : " + port);
});

//user defined middlewares
app.use("/", function (req, res, next) {
    console.log("Request URL : -- >" + req.url +"   @Time :"+Date.now());
    next();
});
// Using middleware for static files
app.use("/assets", express.static(__dirname + "/public/styles"));

// using view engines
app.set("views", "./public/views");
app.set("view engine", "ejs");

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

// simple get method
app.get("/", function (req, res) {
    res.render("index2");
});

app.post("/person",urlencodedParser,function(req,res){
    res.send("Thank You!");
    console.log(req.body.firstName);
    console.log(req.body.lastName);
});

app.post("/personJsonParser",jsonParser,function(req,res){
    res.send("Thank you for json data");
    console.log(req.body.firstName);
    console.log(req.body.lastName);
});