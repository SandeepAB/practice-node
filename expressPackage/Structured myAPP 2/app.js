var express = require("express");
var app = express();
var htmlController = require("./controller/htmlController");
var port = process.env.PORT || 3000

app.listen(port, function () {
    console.log("The server is listening on port : " + port);
});

// Using middleware for static files
app.use("/assets", express.static(__dirname + "/public/styles"));

app.set("views", "./public/views");

app.set("view engine", "ejs");

htmlController(app);



