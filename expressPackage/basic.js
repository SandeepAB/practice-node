var express = require("express");
var app = express();

var port = process.env.PORT || 3000

app.listen(port, function () {
    console.log("The server is listening on port : " + port);
});

//user defined middlewares
app.use("/", function (req, res, next) {
    console.log("Request URL" + req.url);
    next();
});

// Using middleware for static files
app.use("/assets", express.static(__dirname + "/public/styles"));

// simple get method
app.get("/", function (req, res) {
    res.send("<html><body><h1>Hello World</h1></body></html>");
});

//sending JSONs
app.get("/api", function (req, res) {
    var obj = {
        fName: "Sandeep",
        lName: "Basetti"
    }
    res.json(obj);
});

//Get with parameters
app.get("/person/:id", function (req, res) {
    res.send("<html><body><h1>Person :" + req.params.id + "</h1></body></html>")
});

//Get with multiple parameters
app.get("/fullname/:firstname/:lastname", function (req, res) {
    res.send("<html><body><h1>" + req.params.lastname + " " + req.params.firstname + "</h1></body></html>")
});

//Using middlewares 

app.get("/testMiddleware", function (req, res) {
    res.send("<html><link href='assets/style.css' tyep=text/css rel=stylesheet></link><body><h1>Test Middleware</h1></body></html>");
});

//======================
// using view engines
//======================

app.set("views", "./public/views");

app.set("view engine", "ejs");

app.get("/viewEngine", function (req, res) {
    res.render("index", { ID: "Sandeep" });
});

app.get("/viewEngineParams/:id", function (req, res) {
    res.render("index", { ID: req.params.id ,Qstr:req.query.Qstr });
});

